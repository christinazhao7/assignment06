/*
* INFO/CS 1300
* Fall 2016
* Christina Zhao
*
*/

/* brownies image adapted from http://4.bp.blogspot.com/-2kPaEenEwZ0/UPyiI6oIFdI/AAAAAAAADTE/GlqmFB_7UPM/s1600/hero-chewy-fudge-premium-brownie-mix.jpg
ice cream image adapted from http://img.webmd.com/dtmcms/live/webmd/consumer_assets/site_images/sponsored_programs/r_fit/kids/content_images/fitkids_quiz_icecreamquiz_w1.jpg
creme brulee image adapted from http://images-gmi-pmc.edge-generalmills.com/f77dabc2-30c3-4910-885b-36b3fab00b43.jpg
banana bread image adapted from http://www.simplyrecipes.com/recipes/banana_bread/ */

//array of images
var dessert_images = ["images/image1.JPG", "images/image2.jpg", "images/image3.jpg", "images/image4.jpg"];

var ind = 0;
var end = dessert_images.length - 1;


function changeImage(index) {
    "use strict";
    var myimage = document.getElementById('main_image');
    
    ind = ind + index; //update ind for previous or next
    
    //wrap to beginning for next button
    if (ind > end) {
        ind = 0;
    }
    
    //wrap to end for previous button
    if (ind < 0) {
        ind = end;
    }

    myimage.src = dessert_images[ind];
    
}